#include<stdio.h>
void Swap_callby_ref(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
printf("(in function)The values after swapping are a=%d and b=%d\n", *a,*b);
}
int main()
{
int a,b;
printf("Enter two numbers\n");
scanf("%d%d",&a,&b);
printf("The values before swapping are a=%d and b=%d\n",a,b);
Swap_callby_ref(&a,&b);
printf("IN MAIN The values are a=%d and b=%d\n",a,b);
return 0;
}